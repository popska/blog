;;; -*- lexical-binding: t -*-

;; thanks https://systemcrafters.net/publishing-websites-with-org-mode/building-the-site/

(require 'ox-publish)
(require 'package)
(setq package-user-dir (expand-file-name "./.packages"))
(setq package-archives '(("melpa" . "https://melpa.org/packages/")
                         ("elpa" . "https://elpa.gnu.org/packages/")))

;; Initialize the package system
(package-initialize)
(unless package-archive-contents
  (package-refresh-contents))

;; Install htmlize for coloring code blocks
(unless (package-installed-p 'htmlize)
  (package-install 'htmlize))

;; name and email used during export
(setq user-full-name "Marcus Quincy"
      user-mail-address "blog@marcusquincy.org")

(defun m-extract-preview (filepath)
  "Gets first part of paragraph from org file"
  (with-temp-buffer
    (insert-file-contents filepath)
    (goto-char (point-min))
    ;; find first line not starting with * or # which is at least 50 chars long
    (flush-lines "^\*\\|^#\\|^$")
    (concat
     (buffer-substring-no-properties (line-beginning-position)
                                     (min (line-end-position) (+ (line-beginning-position) 100)))
     "...")))

;; override sitemap entry function for index.org
;; https://taingram.org/blog/org-mode-blog.html
(defun m-org-sitemap-index-format (entry style project)
  "Link the posts with a preamble"
  (let ((post-title (org-publish-find-title entry project)))
    (if (= (length post-title) 0)
        (format "*%s*" entry)
      (format "{{{preview(%s,%s,%s,%s)}}}\n\n"
              (concat (file-name-sans-extension entry) ".html")
              post-title
              (format-time-string "%Y-%m-%d"
                                  (org-publish-find-date entry project))
              (string-replace "," "__COMMA__"
                              (m-extract-preview (expand-file-name entry "content/posts")))))))

(defun m-fix-commas (orig-file new-file)
  "replace __COMMA__ with , in NEW-FILE when needed"
  (when (string= (file-name-nondirectory orig-file) "index.org")
    (with-temp-buffer
      (insert-file-contents new-file)
      (goto-char (point-min))
      (while (search-forward "__COMMA__" nil t)
        (replace-match ","))
      (write-region (point-min) (point-max) new-file nil))))

(add-hook 'org-publish-after-publishing-hook #'m-fix-commas)

;; make it so index sitemap isn't a <ul>, we just want the "html" string
;; TODO: limit to only last 5 or so posts
(defun m-publish-index (title list)
  "Function that generates the sitemap"
  (concat "#+TITLE: " title "\n\n"
          (apply #'concat (mapcar #'car (cdr list)))))

(defun m-org-sitemap-tag-format (entry style project)
  "Link the posts with a preamble"
  (let* ((filename (org-publish-find-title entry project))
         (tags (org-publish-find-property entry :filetags project))
         (link (format
                "\"- (%s) [[file:%s][%s]]\n\""
                (format-time-string "%Y-%m-%d"
                                    (org-publish-find-date entry project))
                entry
                filename)))
    (if (not tags)
        (format "%s" (list (cons "\"Untagged\"" link)))
      (format "%s"
              (mapcar (lambda (tag) (cons tag link)) tags)))))

;; TODO sort tags alphabetically and within tags by date with untagged at very end
(defun m-org-sitemap-tag-publish (title list)
  "Consolidates tags from all the files"
  (let ((tags '())
        (links (mapcar (lambda (file-tags) (read (car file-tags)))
                       (cdr list))))
    (dolist (pair (apply #'append links))
      (setf (alist-get (car pair) tags nil 'remove #'string=)
            (cons (cdr pair) (alist-get (car pair) tags nil nil #'string=))))
    (concat "#+TITLE: " title "\n\n"
            (apply #'concat
                   (mapcar (lambda (tag-entries)
                             (format "* %s\n%s" (car tag-entries)
                                     (apply #'concat (cdr tag-entries))))
                           tags)))))

(setq org-export-global-macros
      `(("preview" . ,(concat "#+BEGIN_EXPORT html\n"
                              (with-temp-buffer (insert-file-contents "./content/static/preview.html") (buffer-string))
                              "\n#+END_EXPORT"))))

;; advise org-html-format-spec to have any other items i would want, such as the org file name for gitlab link
(define-advice org-html-format-spec (:override (info) mine)
  ;; default
  "Return format specification for preamble and postamble.
INFO is a plist used as a communication channel."
  ;; (print-plist-keys info)
  (let ((timestamp-format (plist-get info :html-metadata-timestamp-format)))
    `((?t . ,(org-export-data (plist-get info :title) info))
      (?s . ,(org-export-data (plist-get info :subtitle) info))
      (?d . ,(org-export-data (org-export-get-date info timestamp-format)
                              info))
      (?T . ,(format-time-string timestamp-format))
      (?a . ,(org-export-data (plist-get info :author) info))
      (?e . ,(mapconcat
              (lambda (e) (format "<a href=\"mailto:%s\">%s</a>" e e))
              (split-string (plist-get info :email)  ",+ *")
              ", "))
      (?c . ,(plist-get info :creator))
      (?C . ,(let ((file (plist-get info :input-file)))
               (format-time-string timestamp-format
                                   (and file (file-attribute-modification-time
                                              (file-attributes file))))))
      (?v . ,(or (plist-get info :html-validation-link) ""))
      ;; add my own formatters here
      (?G . ,(let ((file (file-name-nondirectory (plist-get info :input-file))))
               (if (member file '("index.org" "sitemap.org" "tagmap.org"))
                   (format (concat "<p>View the "
                                   "<a href=\"https://gitlab.com/popska/blog\">blog source code</a> on GitLab.</p>")
                           file)
                 (format (concat "<p>View the "
                                 "<a href=\"https://gitlab.com/popska/blog/-/commits/master/content/posts/%s?ref_type=heads\">post changelog</a>"
                                 " and <a href=\"https://gitlab.com/popska/blog\">blog source code</a> on GitLab.</p>")
                         file)))))))

;; Define the publishing project
(setq org-publish-project-alist
      (list
       (list "index"
             :recursive t
             :base-directory "./content/posts"
             :base-extension "org"
             :publishing-directory "./public" ; all posts published here are overwritten by "posts"
             :publishing-function 'org-html-publish-to-html
             :language "en"
             :exclude (concat "index.org\\|tagmap.org\\|sitemap.org"
                              (if (string= (getenv "DEVELOPMENT") "t") nil "\\|^WIP"))

             ;; There are more things that can be done with site map
             :auto-sitemap t ; generate a file linking to all posts
             :sitemap-filename "index.org"
             :sitemap-title "Marcus Quincy's Blog"
             :sitemap-sort-files 'anti-chronologically
             :sitemap-format-entry #'m-org-sitemap-index-format
             :sitemap-function #'m-publish-index

             :html-postamble (with-temp-buffer (insert-file-contents "./content/static/postamble.html") (buffer-string))
             )
       (list "tags"
             :recursive t
             :base-directory "./content/posts"
             :base-extension "org"
             :publishing-directory "./public" ; all posts published here are overwritten by "posts"
             :publishing-function 'org-html-publish-to-html
             :language "en"
             :exclude (concat "index.org\\|tagmap.org\\|sitemap.org"
                              (if (string= (getenv "DEVELOPMENT") "t") nil "\\|^WIP"))

             :preserve-breaks t ; include line breaks
             :with-author nil ; include name of author in generated files
             :with-date nil ; use the date from the file itself
             :with-creator t ; creates string saying generated in emacs org mode
             :with-email nil ; export my email
             :with-toc t ; posts should not have table of contents
             :section-numbers nil ; don't number sections
             :time-stamp-file nil ; don't include the date generated

             ;; There are more things that can be done with site map
             :auto-sitemap t ; this "sitemap" will be sorted by tag
             :sitemap-filename "tagmap.org"
             :sitemap-title "Tags"
             :sitemap-sort-files 'anti-chronologically
             :sitemap-format-entry #'m-org-sitemap-tag-format
             :sitemap-function #'m-org-sitemap-tag-publish

             :html-preamble (with-temp-buffer (insert-file-contents "./content/static/preamble.html") (buffer-string))
             :html-postamble (with-temp-buffer (insert-file-contents "./content/static/postamble.html") (buffer-string))
             )
       (list "posts"
             :recursive t
             :base-directory "./content/posts"
             :base-extension "org"
             :publishing-directory "./public"
             :publishing-function 'org-html-publish-to-html
             :language "en"
             :exclude (concat "index.org\\|tagmap.org\\|sitemap.org"
                              (if (string= (getenv "DEVELOPMENT") "t") nil "\\|^WIP")) ; don't publish files starting with string WIP

             :preserve-breaks t ; include line breaks
             :with-author t ; include name of author in generated files
             :with-date t ; use the date from the file itself
             :with-creator t ; creates string saying generated in emacs org mode
             :with-email t ; export my email
             :with-toc nil ; posts should not have table of contents
             :section-numbers nil ; don't number sections
             :time-stamp-file nil ; don't include the date generated

             ;; There are more things that can be done with site map
             :auto-sitemap t ; generate a file linking to all posts
             :sitemap-filename "sitemap.org"
             :sitemap-title "Archive"
             :sitemap-sort-files 'anti-chronologically

             ;; add header and footer
             :html-preamble (with-temp-buffer (insert-file-contents "./content/static/preamble.html") (buffer-string))
             :html-postamble (with-temp-buffer (insert-file-contents "./content/static/postamble.html") (buffer-string))
             )
       (list "static"
             :recursive t
             :base-directory "./content/static"
             :base-extension "css\\|js\\|png\\|jpg\\|jpeg"
             :publishing-directory "./public"
             :publishing-function 'org-publish-attachment)
       (list "pages" ; meant for custom pages that aren't posts (i.e. an "about me" page)
             :recursive nil ; this is at root
             :base-directory "./content"
             :base-extension "org"
             :publishing-directory "./public"
             :publishing-function 'org-html-publish-to-html
             :language "en")
       (list "all"
             :components '("index" "tags" "posts" "static" "pages"))))

(setq org-html-validation-link nil            ;; Don't show validation link
      org-html-head-include-scripts nil       ;; Use our own scripts
      org-html-head-include-default-style nil ;; Use our own styles
      org-html-head "<link rel=\"stylesheet\" href=\"https://cdn.simplecss.org/simple.min.css\" />
<link rel=\"stylesheet\" href=\"./style.css\" />")

;; Generate the site output
(org-publish-project "all" t)

(message "Build complete!")
