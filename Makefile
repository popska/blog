build: build-site.el $(wildcard content/*)
	emacs -Q --script build-site.el

clean:
	rm -rf public
