#!/usr/bin/env bash

make build

# https://superuser.com/questions/181517/how-to-execute-a-command-whenever-a-file-changes
inotifywait -r -e close_write,moved_to,create -m content ./*.el Makefile |
    while read -r directory events filename; do
        # filter out emacs tmp files
        if ! [[ "$filename" =~ \#|index.org|sitemap.org|tagmap.org ]]; then
            echo $filename changed... running make
            make build
        fi
    done
