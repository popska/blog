FROM alpine:3.20.0
# install emacs without gui for converting org, make, bash for watch.sh script, and inotify-tools as file watcher
RUN apk update && apk add emacs-nox && apk add make && apk add inotify-tools && apk add bash && apk add git
